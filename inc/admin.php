<?php
if ( ! function_exists( 'wp_handle_upload' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/file.php' );
}

function nvnzps_menu() {
	add_menu_page( 'Synchronizacja produktów', 'Synchronizacja produktów', 'manage_options', 'nvnz_sync', 'nvnz_sync', 'dashicons-sos');
}

function nvnz_sync() {
	if(! current_user_can('manage_options')){
		wp_die(__('You do not have sufficient permissions to access this page'));
	}

	echo '<div class="wrap">';
	echo '<h1>Synchronizacja produktów</h1>';


	if (isset($_POST['clear'])) {
		nvnz_clear_products();
		echo '<p>Wszystkie produkty zostały usunięte.</p>';
	}


	if (isset($_POST['save'])) {
		nvnz_readcsv($_POST['file']);
		echo "Produkty zsynchronizowane";
	}else	if (isset($_POST['upload'])) {
		$uploadedfile = $_FILES['csv'];

		$upload_overrides = array( 'test_form' => false );

		$movefile = wp_handle_upload( $uploadedfile, $upload_overrides );

		if ( $movefile && ! isset( $movefile['error'] ) ) {
		    echo "Plik prawidłowy i gotowy do synchronizacji.\n";
				echo '<form method="post" enctype="multipart/form-data">';
				echo '<input type="hidden" name="file" value="'.$movefile['file'].'" />';
				echo '<p class="submit">
					<input type="submit" name="save" id="submit" class="button button-primary" value="Synchronizuj produkty"  />
					</p>';
					echo '</form>';
		} else {

		    echo $movefile['error'];
		}
	}
	else {
		echo '<form method="post" enctype="multipart/form-data">';
		echo '<table class="form-table">';
		echo '	<tr>
			<th scope="row"><label for="csv">Plik CSV z produktami</label></th>
			<td><input name="csv" type="file" id="csv" class="regular-file" /></td>
			</tr>';
			echo '</table>';
		echo '<p class="submit">
			<input type="submit" name="upload" id="submit" class="button button-primary" value="Wgraj plik"  />
			<input type="submit" name="clear" id="submit" class="button button-danger" value="Wyczyść wszystkie produkty"  />
			</p>';
			echo '</form>';
	}

	echo '</div>';
}

function nvnz_clear_products() {
	global $wpdb;

	$wpdb->query('DELETE FROM wp_posts');
	$wpdb->query('DELETE FROM wp_postmeta');
}

function nvnz_readcsv($file) {
	$data = array();

	$i = 0;

	if (($uchwyt = fopen ($file,"r")) !== FALSE) {
		while (($db = fgetcsv($uchwyt, 100000, ",")) !== FALSE)  {
			if ($i++ === 0)
				continue;

        //if ($i == 5)
      //  break;

				if (!isset($data[$db[1]])) {
					$i2 = 0;
					$data[$db[1]] = array();
					//$data[$db[1]]['category'] = $db[0];
					//$data[$db[1]]['category'] = $db[1];
					$data[$db[1]]['name'] = $db[1];
					$data[$db[1]]['height'] = $db[9];
					$data[$db[1]]['width'] = $db[11];
					$data[$db[1]]['length'] = $db[12];
					$data[$db[1]]['quantity'] = floor(preg_replace('#([^0-9\.]*)#', '', str_replace(',', '.', $db[16])));
					$data[$db[1]]['sku'] = $db[18];
					$data[$db[1]]['price'] = str_replace(',', '.', $db[20]);

					$data[$db[1]]['attributes'] = array();
					$data[$db[1]]['variations'] = array();

					$variation = array();
					$variation['attributes'] = array();

					if (!empty(trim($db[2])))
					{
						$slug = sanitize_title($db[2]);

						$data[$db[1]]['attributes'][$slug] = array(
							'name' => $db[2],
							'value' => array($db[3]),
							'position' => '1',
							'is_visible' => '1',
							'is_variation' => '1',
							'is_taxonomy' => '0'
						);

						$variation['attributes']['attribute_'.$slug] = $db[3];
					}

					if (!empty(trim($db[4])))
					{
						$slug = sanitize_title($db[4]);

						$data[$db[1]]['attributes'][$slug] = array(
							'name' => $db[4],
							'value' => array($db[5]),
							'position' => '1',
							'is_visible' => '1',
							'is_variation' => '1',
							'is_taxonomy' => '0'
						);

						$variation['attributes']['attribute_'.$slug] = $db[5];
					}

					if (!empty(trim($db[8])))
					{
						$slug = sanitize_title('Struktura');

						$data[$db[1]]['attributes'][$slug] = array(
							'name' => 'Struktura',
							'value' => array($db[8]),
							'position' => '1',
							'is_visible' => '1',
							'is_variation' => '1',
							'is_taxonomy' => '0'
						);

						$variation['attributes']['attribute_'.$slug] = $db[8];
					}

					if (!empty(trim($db[10])))
					{
						$slug = sanitize_title('Kolor');

						$data[$db[1]]['attributes'][$slug] = array(
							'name' => 'Kolor',
							'value' => array($db[10]),
							'position' => '1',
							'is_visible' => '1',
							'is_variation' => '1',
							'is_taxonomy' => '0'
						);

						$variation['attributes']['attribute_'.$slug] = $db[10];
					}


					$variation['height'] = $db[9];
					$variation['width'] = $db[11];
					$variation['length'] = $db[12];
					$variation['quantity'] = floor(preg_replace('#([^0-9\.]*)#', '', str_replace(',', '.', $db[16])));
					$variation['sku'] = $db[18];
					$variation['price'] = $db[20];
					$variation['name'] = $db[1];

					$variation['variations'][] = $variation;
				}
				else {
					$variation = array();
					$variation['attributes'] = array();

					if (!empty(trim($db[2])))
					{
						$slug = sanitize_title($db[2]);

						if (!in_array($db[3], $data[$db[1]]['attributes'][$slug]['value']))
						{
							$data[$db[1]]['attributes'][$slug]['value'][] = $db[3];
						}

						$variation['attributes']['attribute_'.$slug] = $db[3];
					}

					if (!empty(trim($db[4])))
					{
						$slug = sanitize_title($db[4]);

						if (!in_array($db[5], $data[$db[1]]['attributes'][$slug]['value']))
						{
							$data[$db[1]]['attributes'][$slug]['value'][] = $db[5];
						}

						$variation['attributes']['attribute_'.$slug] = $db[5];
					}

					if (!empty(trim($db[8])))
					{
						$slug = sanitize_title('Struktura');

						if (!in_array($db[8], $data[$db[1]]['attributes'][$slug]['value']))
						{
							$data[$db[1]]['attributes'][$slug]['value'][] = $db[8];
						}

						$variation['attributes']['attribute_'.$slug] = $db[8];
					}

					if (!empty(trim($db[10])))
					{
						$slug = sanitize_title('Kolor');

						if (!in_array($db[10], $data[$db[1]]['attributes'][$slug]['value']))
						{
							$data[$db[1]]['attributes'][$slug]['value'][] = $db[10];
						}

						$variation['attributes']['attribute_'.$slug] = $db[10];
					}

					$variation['height'] = $db[9];
					$variation['width'] = $db[11];
					$variation['length'] = $db[12];
					$variation['quantity'] = floor(preg_replace('#([^0-9\.]*)#', '', str_replace(',', '.', $db[16])));
					$variation['sku'] = $db[18];
					$variation['price'] = str_replace(',', '.', $db[20]);
					$variation['name'] = $db[1];

					$data[$db[1]]['variations'][] = $variation;

					$i2++;
				}

				/*$data[$db[1]][$i] = array(


					'attributes' => array(
						array(
							'name' => $db[2],
							'value' => $db[3],
						),
						array(
							'name' => $db[3],
							'value' => $db[4],
						),
						array(
							'name' => 'Struktura',
							'value' => $db[8],
						)
					),

					// ...

					//'grouper' => $db[1], // prawdopodobnie tego uzyjemy do grupowania produktu

					// attributes
					'height' => $db[9], // grubosc

					'width' => $db[11],
					'length' => $db[12],
					// end attributes

					'size' => $db[13], // polaczenie dwoch wyzej z WxL
					'jm' => $db[14], // jednostka miary, brak zastosowania w woo?
					'zamow' => $db[15], // ??
					'quantity_w_unit' => $db[16], // szt/m2 - jak to zastosowac?
					//'status' => $db[17], // spr/spz?
					'code' => $db[18], // tego uzyjemy do synchronizacji
					'ean' => $db[19],
					'price' => $db[20],
					//'quanity_raw' => $db[21],
					//'aa' => $db[22], // aa ??
					//'rezerw' => $db[23], // odliczac od quantity ogolnego?
				);*/
		}
		fclose ($uchwyt);
	}
  $added = array();
  $synced = array();
	foreach ($data as $d)
  {
    $ret = nvnz_sync_product($d);

    $added = array_merge($added, $ret['added']);
    $synced = array_merge($synced, $ret['synced']);

	}
  $added_count = count( $added);
  $synced_count = count($synced);

  $all = array_merge($added, $synced);

  $deleted_count = nvnz_delete_deleted($all);

  echo 'Dodane: '. $added_count.'<br />';
  echo 'Zaktualizowane: '.$synced_count.'<br />';
  echo 'Usunięte: '.$deleted_count.'<br />';

}

function nvnz_find_product($sku) {
  global $wpdb;

  return $wpdb->get_var( "SELECT post_id FROM $wpdb->postmeta WHERE meta_key='_sku' AND meta_value='".$sku."'");
}

function nvnz_delete_deleted(array $synced) {
  global $wpdb;

    $to_delete = $wpdb->get_results( "SELECT ID FROM $wpdb->posts WHERE ID NOT IN (".implode(",", $synced).")");

    $i = 0;
    foreach ($to_delete as $post)
    {
      wp_delete_post( $post->ID, true);

      $i++;
    }

    return $i;
}


function nvnz_sync_product(array $data) {
  $r = array('synced' => array(), 'added' => array());
	$post = array(
    'post_author' => get_current_user_id(),
    'post_content' => '',
    'post_status' => "publish",
    'post_title' => $data['name'],
    'post_parent' => '',
    'post_type' => "product",
	);

  $founded_id = nvnz_find_product($data['sku']);

  if ($founded_id != null)
  {
    update_post_meta( $founded_id, '_sku', $data['sku']); // ean
    // setted by quantity value
    update_post_meta( $founded_id, '_stock', $data['quantity'] );
    if ($data['quantity'] > 0)
    {
      update_post_meta( $founded_id, '_stock_status', 'instock');
    }
    else {
      update_post_meta( $founded_id, '_stock_status', 'outofstock');
    }

    update_post_meta( $founded_id, '_regular_price', $data['price'] );
    update_post_meta( $founded_id, '_price', $data['price'] );


    $r['synced'][] = $founded_id;
    $post_id = $founded_id;
  }
  else
  {
	$post_id = wp_insert_post( $post, $wp_error );
  $r['added'][] = $post_id;
	wp_set_object_terms($post_id, 'variable', 'product_type');

	// set category
	//wp_set_object_terms( $post_id, $data['category'], 'product_cat' );
	// from document
	update_post_meta( $post_id, '_weight', 0 );
	update_post_meta( $post_id, '_length', $data['length'] );
	update_post_meta( $post_id, '_width', $data['width'] );
	update_post_meta( $post_id, '_height', $data['height'] );
	update_post_meta( $post_id, '_sku', $data['sku']); // ean
	// setted by quantity value
	if ($data['quantity'] > 0)
	{
		update_post_meta( $post_id, '_stock_status', 'instock');
	}
	else {
		update_post_meta( $post_id, '_stock_status', 'outofstock');
	}
	update_post_meta( $post_id, '_stock', $data['quantity'] );
	// hide if out of stock?
	update_post_meta( $post_id, '_visibility', 'visible' );
	// prices
	update_post_meta( $post_id, '_regular_price', $data['price'] );
	update_post_meta( $post_id, '_price', $data['price'] );

	// prepared attributes table
	foreach ($data['attributes'] as &$attribute) {
		$attribute['value'] = implode(' | ', $attribute['value']);
	}
	update_post_meta( $post_id, '_product_attributes', $data['attributes']);

	// unused
	update_post_meta( $post_id, 'total_sales', '0');
	update_post_meta( $post_id, '_downloadable', 'no');
	update_post_meta( $post_id, '_virtual', 'no');
	update_post_meta( $post_id, '_purchase_note', "" );
	update_post_meta( $post_id, '_featured', "no" );
	update_post_meta( $post_id, '_manage_stock', "yes" );
	update_post_meta( $post_id, '_default_attributes', array());
	update_post_meta( $post_id, '_sale_price_dates_from', "" );
	update_post_meta( $post_id, '_sale_price_dates_to', "" );
	update_post_meta( $post_id, '_sold_individually', "" );
	update_post_meta( $post_id, '_backorders', "no" );
	update_post_meta( $post_id, 'disable_variations_table', "" );
	update_post_meta( $post_id, 'disable_variations_table_header', "" );
	update_post_meta( $post_id, 'disable_variations_table_offer', "" );
	update_post_meta( $post_id, 'custom_variations_table_header_skip', "" );
	update_post_meta( $post_id, 'custom_variations_table_header', "" );
	update_post_meta( $post_id, 'custom_variations_preordering', "custom" );
	update_post_meta( $post_id, 'custom_variations_preordering_direction', "custom" );
	update_post_meta( $post_id, 'custom_variations_table_header_skip', "" );
	update_post_meta( $post_id, '_min_variation_price', "" );
	update_post_meta( $post_id, '_max_variation_price', "" );
	update_post_meta( $post_id, '_min_price_variation_id', "" );
	update_post_meta( $post_id, '_max_price_variation_id', "" );
	update_post_meta( $post_id, '_min_variation_regular_price', "" );
	update_post_meta( $post_id, '_min_regular_price_variation_id', "" );
	update_post_meta( $post_id, '_max_regular_price_variation_id', "" );
	update_post_meta( $post_id, '_min_variation_sale_price', "" );
	update_post_meta( $post_id, '_max_variation_sale_price', "" );
	update_post_meta( $post_id, '_min_sale_price_variation_id', "" );
	update_post_meta( $post_id, '_max_sale_price_variation_id', "" );
	update_post_meta( $post_id, '_product_image_gallery', '');
}
	$i = 1;
	foreach ($data['variations'] as $v) {
	   nvnz_add_variant($post_id, $v, $i, $r);
		$i++;
	}

  return $r;
}

function nvnz_add_variant($parent_id, array $data, $count = 1, &$r) {

	$post = array(
    'post_author' => get_current_user_id(),
    'post_content' => '',
    'post_status' => "publish",
    'post_title' => 'Wariant #'.$count.' z '.$data['name'],
    'post_parent' => $parent_id,
    'post_type' => "product_variation",
	);

  $founded_id = nvnz_find_product($data['sku']);

  if ($founded_id != null)
  {
    update_post_meta( $founded_id, '_sku', $data['sku']); // ean
    // setted by quantity value
    if ($data['quantity'] > 0)
    {
      update_post_meta( $founded_id, '_stock_status', 'instock');
    }
    else {
      update_post_meta( $founded_id, '_stock_status', 'outofstock');
    }

    update_post_meta( $founded_id, '_regular_price', $data['price'] );
    update_post_meta( $founded_id, '_price', $data['price'] );
    update_post_meta( $founded_id, '_stock', $data['quantity'] );

    $r['synced'][] = $founded_id;
  }
  else
  {

	//Create post
	$post_id = wp_insert_post( $post, $wp_error );
  $r['added'][] = $post_id;

	wp_set_object_terms($post_id, 'simple', 'product_type');

	update_post_meta( $post_id, '_visibility', 'visible' );
	if ($data['quantity'] > 0)
	{
		update_post_meta( $post_id, '_stock_status', 'instock');
	}
	else {
		update_post_meta( $post_id, '_stock_status', 'outofstock');
	}
	//update_post_meta( $post_id, 'total_sales', '0');
	update_post_meta( $post_id, '_downloadable', 'no');
	update_post_meta( $post_id, '_virtual', 'no');

	//update_post_meta( $post_id, '_purchase_note', "" );
	//update_post_meta( $post_id, '_featured', "no" );
	update_post_meta( $post_id, '_weight','' );
	update_post_meta( $post_id, '_length', $data['length'] );
	update_post_meta( $post_id, '_width', $data['width'] );
	update_post_meta( $post_id, '_height', $data['height'] );
	update_post_meta( $post_id, '_sku', $data['sku']);
	//update_post_meta( $post_id, '_default_attributes', array());


	update_post_meta( $post_id, '_sale_price_dates_from', "" );
	update_post_meta( $post_id, '_sale_price_dates_to', "" );
	update_post_meta( $post_id, '_sale_price', "" );
	update_post_meta( $post_id, '_price',$data['price'] );
	update_post_meta( $post_id, '_regular_price', $data['price'] );

	update_post_meta( $post_id, '_manage_stock', "yes" );
	update_post_meta( $post_id, '_stock', $data['quantity'] );

	update_post_meta( $post_id, 'enbable_variations_table_img', "" );
	update_post_meta( $post_id, 'override_extra_image', "" );
	update_post_meta( $post_id, 'vt_variation_description', "" );
	update_post_meta( $post_id, 'vartable_qty_step', "" );
	update_post_meta( $post_id, 'vartable_qty_default', "" );
	update_post_meta( $post_id, '_variation_description', "" );

	update_post_meta( $post_id, '_thumbnail_id', '0');

	foreach ($data['attributes'] as $at_name => $at_value) {
		update_post_meta( $post_id, $at_name, $at_value);
	}
}


}

add_action( 'admin_menu' , 'nvnzps_menu' );
