<?php
function nvnzps_install_create_formdata_db() {
	global $wpdb;

	$charset_collate = $wpdb->get_charset_collate();
	$table_name = $wpdb->prefix . 'nvnzps_forms_data';

	$sql = "CREATE TABLE $table_name (
		id mediumint(9) NOT NULL AUTO_INCREMENT,
		time datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
		formdata text,
		formtype varchar(20),
		status varchar(20) NOT NULL,
		user_ID bigint(20) NOT NULL,
		step2id char(32) NOT NULL,
		mail_sent varchar(2) DEFAULT 'n' NOT NULL,
		UNIQUE KEY id (id)
	) $charset_collate;";

	dbDelta( $sql );
}

function nvnzps_uninstall_drop_formdata_db(){
	global $wpdb;
	$table = $wpdb->prefix."nvnzps_forms_data";

	//Delete any options thats stored also?
	//delete_option('wp_plugin_version');

	$wpdb->query("DROP TABLE IF EXISTS $table");
}

function nvnzps_install_create_partnersfiles_db() {
	global $wpdb;

	$charset_collate = $wpdb->get_charset_collate();
	$table_name = $wpdb->prefix . 'nvnzps_partners_files';

	$sql = "CREATE TABLE $table_name (
		id mediumint(9) NOT NULL AUTO_INCREMENT,
		time datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
		user_ID bigint(20) NOT NULL,
		file varchar(200) NOT NULL,
		description varchar(200) NOT NULL,
		UNIQUE KEY id (id)
	) $charset_collate;";

	dbDelta( $sql );
}

function nvnzps_uninstall_drop_partnersfiles_db(){
	global $wpdb;
	$table = $wpdb->prefix."nvnzps_partners_files";

	//Delete any options thats stored also?
	//delete_option('wp_plugin_version');

	$wpdb->query("DROP TABLE IF EXISTS $table");
}

function nvnzps_install_create_formtempdata_db() {
	global $wpdb;

	$charset_collate = $wpdb->get_charset_collate();
	$table_name = $wpdb->prefix . 'nvnzps_form_temp_data';

	$sql = "CREATE TABLE $table_name (
		user_ID bigint(20) NOT NULL,
		formdata text,
		formtype varchar(20),
		partnerscount smallint(5) NOT NULL,
		UNIQUE KEY user_id_formtype (user_ID,formtype)
	) $charset_collate;";

	dbDelta( $sql );
}

function nvnzps_uninstall_drop_formtempdata_db(){
	global $wpdb;
	$table = $wpdb->prefix."nvnzps_form_temp_data";

	//Delete any options thats stored also?
	//delete_option('wp_plugin_version');

	$wpdb->query("DROP TABLE IF EXISTS $table");
}
