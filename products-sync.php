<?php
/*
Plugin Name: Synchronizacja produktów
Plugin URI: http://invenze.com/
Description: Synchronizacja produktów
Version: 1.0.0
Author: Invenze
Author URI: invenze.com
License: GPLv2 or later
*/

include_once 'inc/install.php';

// menu.php in tutorial
include_once 'inc/admin.php';


/* installation */
function nvnzps_install() {

}

register_activation_hook( __FILE__, 'nvnzps_install');
/* installation END */


/* deinstall */
function nvnzps_uninstall(){

}
// uninstalling disabled to prevent delete data
//register_deactivation_hook( __FILE__, 'nvnzps_uninstall' );
/* deinstall end */

add_action( 'plugins_loaded', 'loadScripts');

function loadScripts(){
    add_action( 'wp_enqueue_scripts', 'robelit_enqueue_style' );
    add_action( 'wp_enqueue_scripts', 'robelit_enqueue_script' );
}

function robelit_enqueue_style() {
     wp_enqueue_style( 'core', plugin_dir_url( __FILE__ ) . 'assets/style.filterable.css', false );
}

function robelit_enqueue_script() {
    wp_enqueue_script( 'my-js', plugin_dir_url( __FILE__ ) . 'assets/jquery.filterable.modified.js', false, false, true );
}
